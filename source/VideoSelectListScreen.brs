' +---------------------------------------------------------------------------+
' | Skoobalon Personal Roku Channel                                           |
' | Copyright (C) 2013 Steven T. Wallace                                      |
' |                                                                           |
' | This program is free software; you can redistribute it and/or modify      |
' | it under the terms of the GNU General Public License as published by      |
' | the Free Software Foundation; version 2 of the License.                   |
' |                                                                           |
' | This program is distributed in the hope that it will be useful,           |
' | but WITHOUT ANY WARRANTY; without even the implied warranty of            |
' | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
' | GNU General Public License for more details.                              |
' |                                                                           |
' | You should have received a copy of the GNU General Public License along   |
' | with this program; if not, write to the Free Software Foundation, Inc.,   |
' | 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.               |
' +---------------------------------------------------------------------------+


' Screen which displays all videos
Function VideoSelectListScreen(url as String, title as String) 
	this = {}
	
	' Constructor
	this.Init = Function(url as String, title as String)
		Print "VideoSelectListScreen.Init()"
		m.port = CreateObject("roMessagePort")
		m.screen = CreateObject("roListScreen")
		m.screen.SetMessagePort(m.port)
		m.screen.SetBreadcrumbText(title, "")

		m.urlStack = CreateObject("roArray", 10, true)
		m.LoadData(url, title)
	End Function
	
	this.LoadData = Function(url as String, title as String)
		' Load XML at root url requested
		m.urlStack.Push(url)
		m.rawXml = m.LoadXml(url)
		If m.rawXml = Invalid
			Print "Error reading xml"
			Return Invalid
		End If
		
		m.albumList = m.ReadAlbumList(m.rawXml)
		If m.albumList <> Invalid
			' This is a directory tree, show albums available
			m.screen.SetContent(m.albumList)
		Else
			' Must be an endpoint then -- get the videos listed and show episode select screen
			videoList = m.ReadVideoList(m.rawXml)
			If videoList <> Invalid
				EpisodeSelectScreen(title, videoList, m.rawXml).Show()
			Else
				' Show error dialog
				NewSimpleDialog("Oops!", "Looks like this album has no videos.", false, ["OK"]).Show()
			End If
			
			' Upon returning, we'll need to reload the data from the previous screen
			m.urlStack.Pop()	' Pops the episode screen from the stack
			m.LoadData(m.urlStack.Pop(), "")
			m.screen.SetFocusedListItem(m.focusedIndex)
		End If
	End Function
	
	this.Show = Function()
		Print "VideoSelectListScreen.Show()"
		m.screen.Show()
		
		While True
			msg = wait(0, m.port)
			If Type(msg) = "roListScreenEvent"
				If msg.IsScreenClosed()
					print "VideoSelectListScreen closed"
					Exit While
				Else If msg.IsRemoteKeyPressed()
					If msg.GetIndex() = 4
						' Left button
						m.urlStack.Pop()
						If m.urlStack.Count() = 0
							' No items left in the stack, close the screen
							m.screen.Close()
							Exit While
						Else
							' Still have items in the stack, use the one underneath
							m.LoadData(m.urlStack.Pop(), "")
						End If
					Else If msg.GetIndex() = 5
						' Right button
						thisData = m.albumList[m.focusedIndex]
						m.LoadData(thisData.ChildUrl, thisData.Title)
					End If
				Else If msg.IsListItemFocused()
					m.focusedIndex = msg.GetIndex()
				Else If msg.IsListItemSelected()
					' Album was selected
					Print "Album Selected: "; msg.GetIndex()
					thisData = m.albumList[msg.GetIndex()]
					m.LoadData(thisData.ChildUrl, thisData.Title)
				End If
			End If
		End While
	End Function
	
	this.ShowVideo = Function(category as Integer, index as Integer)
		vps = VideoPlayerScreen(m.albumList[category].Videos[index])
		vps.Show()
		
		If vps.IsReadyForNext()
			' Video ended without user intervention -- show the next one
			index = index + 1
			If index >= m.albumList[category].Videos.Count()
				index = 0
			End If
			m.screen.SetFocusedListItem(index)
			m.ShowVideo(category, index)
		End If
	End Function
	
	this.LoadXml = Function(url as String) As Object
		url = "http://" + GetSettings().host$ + url
		Print "Loading video XML from "; url
		http = NewHttp(url)
		response = http.GetToStringWithRetry()
		
		xml = CreateObject("roXMLElement")
		If Not xml.Parse(response)
			Print "Cannot parse xml"
			Return Invalid
		End If
		
		Return xml
	End Function
	
	this.ReadAlbumList = Function(xml as Object) As Object
		If xml.album = Invalid
			Print "Improper root element"
			Return Invalid
		Else If xml.album.Count() = 0
			Print "No albums found"
			Return Invalid
		End If
		
		' Build the list of album names and images
		albumList = CreateObject("roArray", 10, true)
		albums = xml.GetChildElements()
		Print "Creating album list"
		For Each album in albums
			albumObj = CreateObject("roAssociativeArray")
			albumObj.Title = album@name
			If album@image <> Invalid
				albumObj.HDPosterUrl = UrlEncode("http://" + GetSettings().host$ + album@image)
			End If
			albumObj.ChildUrl = album@url
			albumList.Push(albumObj)
			Print "- " + albumObj.Title
		End For
		Return albumList
	End Function
	
	this.ReadVideoList = Function(xml as Object) As Object
		If xml.video = Invalid
			Print "Improper root element"
			Return Invalid
		Else If xml.video.Count() = 0
			Print "No videos found"
			Return Invalid
		End If
		
		' Build the list of video names and images
		videoList = CreateObject("roArray", 10, true)
		videos = xml.getChildElements()
		Print "Creating video list"
		
		' Assume all streams have a subtitle track on them, since we currently have no way to track this
		subtitleConfig = { TrackName: "eia608/1" }
		For Each video In videos
			videoObj = CreateObject("roAssociativeArray")
			videoObj.Title = video@name
			videoObj.Description = "-Description-"
			videoObj.ShortDescriptionLine1 = videoObj.Title
			videoObj.ShortDescriptionLine2 = video@description
			videoObj.EpisodeNumber = "-e-"
			videoObj.StreamFormat = "mp4"
			videoObj.StreamBitrates = [1500]
			videoObj.StreamQualities = ["HD"]
			videoObj.StreamUrls = [UrlEncode("http://" + GetSettings().host$ + video@stream)]
			videoObj.HDBifUrl = UrlEncode("http://" + GetSettings().host$ + video@bif)
			videoObj.HDPosterUrl = UrlEncode("http://" + GetSettings().host$ + video@image)
			videoObj.SubtitleConfig = subtitleConfig
			videoList.Push(videoObj)
			Print "- " + videoObj.Title
		End For
		Return videoList
	End Function
		
	this.url = url
	this.title = title
	this.Init(url, title)
	return this
End Function
