' +---------------------------------------------------------------------------+
' | Skoobalon Personal Roku Channel                                           |
' | Copyright (C) 2013 Steven T. Wallace                                      |
' |                                                                           |
' | This program is free software; you can redistribute it and/or modify      |
' | it under the terms of the GNU General Public License as published by      |
' | the Free Software Foundation; version 2 of the License.                   |
' |                                                                           |
' | This program is distributed in the hope that it will be useful,           |
' | but WITHOUT ANY WARRANTY; without even the implied warranty of            |
' | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
' | GNU General Public License for more details.                              |
' |                                                                           |
' | You should have received a copy of the GNU General Public License along   |
' | with this program; if not, write to the Free Software Foundation, Inc.,   |
' | 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.               |
' +---------------------------------------------------------------------------+


' Singleton class

' Gets the singleton instance of the Properties object
Function GetProperties()
	globals = GetGlobalAA()
	If globals._propertiesInstance = Invalid
		globals._propertiesInstance = _NewProperties()
	End If
	Return globals._propertiesInstance
End Function

Function _NewProperties()
	obj = {}
	
	obj.GetProperty = Function(key, defaultValue = invalid)
		value = _Properties_RegRead(key, GetConstants().SETTINGS_SECTION)
		If value = Invalid
			value = defaultValue
		End If
		Return value
	End Function
	
	obj.SetProperty = Function(key, value)
		_Properties_RegWrite(key, value, GetConstants().SETTINGS_SECTION)
	End Function
	
	obj.GetUsername = Function()
		Return m.GetProperty(GetConstants().PROP_USERNAME, "")
	End Function
	
	obj.SetUsername = Function(username)
		m.SetProperty(GetConstants().PROP_USERNAME, username)
		GetSettings().username$ = username
	End Function
	
	obj.GetPassword = Function()
		Return m.GetProperty(GetConstants().PROP_PASSWORD, "")
	End Function
	
	obj.SetPassword = Function(password)
		m.SetProperty(GetConstants().PROP_PASSWORD, password)
		GetSettings().password$ = password
	End Function
	
	obj.GetHostIndex = Function()
		Return m.GetProperty(GetConstants().PROP_HOST_INDEX, 0)
	End Function
	
	obj.SetHostIndex = Function(hostIndex)
		m.SetProperty(GetConstants().PROP_HOST_INDEX, hostIndex)
		GetSettings().host$ = GetConstants().HOSTS[hostIndex]
	End Function
	
	Return obj
End Function


'******************************************************
'Registry Helper Functions
'******************************************************
Function _Properties_RegRead(key, section=invalid)
    If section = Invalid Then section = "Default"
    sec = CreateObject("roRegistrySection", section)
    If sec.Exists(key)
		val = sec.Read(key)
		If val = "True"
			Return True
		Else If val = "False"
			return False
		Else
			intVal = Strtoi(val)
			If intVal <> 0 Or val.Trim() = "0"
				Return intVal
			Else
				Return val
			End If
		End If
	End If
    Return Invalid
End Function

Function _Properties_RegWrite(key, val, section=invalid)
    If section = invalid Then section = "Default"
	If Type(val) = "roBoolean" Or Type(val) = "Boolean"
		If val = True
			val = "True"
		Else 
			val = "False"
		End If
	Else If Type(val) = "roInt" Or Type(val) = "Integer"
		val = Stri(val)
	End If
	print "Section: " section "; " key "=" val
    sec = CreateObject("roRegistrySection", section)
    sec.Write(key, val)
    sec.Flush() 'commit it
End Function

Function _Properties_RegDelete(key, section=invalid)
    if section = invalid then section = "Default"
    sec = CreateObject("roRegistrySection", section)
    sec.Delete(key)
    sec.Flush()
End Function
