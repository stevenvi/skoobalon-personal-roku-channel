' +---------------------------------------------------------------------------+
' | Skoobalon Personal Roku Channel                                           |
' | Copyright (C) 2013 Steven T. Wallace                                      |
' |                                                                           |
' | This program is free software; you can redistribute it and/or modify      |
' | it under the terms of the GNU General Public License as published by      |
' | the Free Software Foundation; version 2 of the License.                   |
' |                                                                           |
' | This program is distributed in the hope that it will be useful,           |
' | but WITHOUT ANY WARRANTY; without even the implied warranty of            |
' | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
' | GNU General Public License for more details.                              |
' |                                                                           |
' | You should have received a copy of the GNU General Public License along   |
' | with this program; if not, write to the Free Software Foundation, Inc.,   |
' | 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.               |
' +---------------------------------------------------------------------------+


' Screen which displays all videos
Function EpisodeSelectScreen(title as String, content as Object, xml as Object)
	this = {}
	
	' Constructor
	this.Init = Function(title as String, content as Object, xml as Object)
		Print "EpisodeSelectScreen.Init()"
		m.port = CreateObject("roMessagePort")
		m.screen = CreateObject("roPosterScreen")
		m.screen.SetMessagePort(m.port)
		m.screen.SetBreadcrumbText(title, "")
		
		If xml@liststyle <> Invalid
			m.screen.SetListStyle(xml@liststyle)
		Else
			m.screen.SetListStyle("flat-category")
		End If
		
		If xml@listdisplaymode <> Invalid
			m.screen.SetListDisplayMode(xml@listdisplaymode)
		Else
			m.screen.SetListDisplayMode("zoom-to-fill")
		End If
		m.screen.SetContentList(content)
	End Function
	
	this.Show = Function()
		Print "EpisodeSelectScreen.Show()"
		m.screen.Show()
		
		While True
			msg = wait(0, m.port)
			If Type(msg) = "roPosterScreenEvent"
				If msg.IsScreenClosed()
					print "EpisodeSelectScreen closed"
					Exit While
				else if msg.IsListItemSelected()
					' Item (video) was selected
					print "Video Selected: "; msg.GetIndex()
					
					' Put up a black screen background first
					black = CreateObject("roImageCanvas")
					black.Show()
					m.ShowVideo(msg.GetIndex())
					black.Close()
				end if
			end if
		End While
	End Function
	
	this.ShowVideo = Function(index as Integer)
		vps = VideoPlayerScreen(m.content[index])
		vps.Show()
		
		If vps.IsReadyForNext()
			' Video ended without user intervention -- show the next one
			index = index + 1
			If index >= m.content.Count()
				index = 0
			End If
			m.screen.SetFocusedListItem(index)
			m.ShowVideo(index)
		End If
	End Function

	this.content = content
	this.Init(title, content, xml)
	return this
End Function