' +---------------------------------------------------------------------------+
' | Skoobalon Personal Roku Channel                                           |
' | Copyright (C) 2013 Steven T. Wallace                                      |
' |                                                                           |
' | This program is free software; you can redistribute it and/or modify      |
' | it under the terms of the GNU General Public License as published by      |
' | the Free Software Foundation; version 2 of the License.                   |
' |                                                                           |
' | This program is distributed in the hope that it will be useful,           |
' | but WITHOUT ANY WARRANTY; without even the implied warranty of            |
' | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
' | GNU General Public License for more details.                              |
' |                                                                           |
' | You should have received a copy of the GNU General Public License along   |
' | with this program; if not, write to the Free Software Foundation, Inc.,   |
' | 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.               |
' +---------------------------------------------------------------------------+


' Singleton class

' Gets the global constants associative array
Function GetConstants()
	globals = GetGlobalAA()
	If globals._constantsInstance = Invalid
		globals._constantsInstance = _NewConstants()
	End If
	Return globals._constantsInstance
End Function

' Populates the global associative array with some constant values
' Should only be called once
Function _NewConstants()
	obj = {
		' Configuration constants
		SETTINGS_SECTION: "skoobalon_personal_roku_channel"
		PROP_HOST_INDEX: "hostIndex"
		PROP_USERNAME: "username"
		PROP_PASSWORD: "password"

		' Mirror list -- useful for a home server, though for a more enterprise
		' style environment, you'd want to get this list from a server, or even
		' query for the mirror at the start of a new session. But this isn't that.
		HOSTS: [ "server1.example.com", "server2.example.com", "server3.example.com" ]
		
		' Path to validate user credentials against
		VALIDATION_PATH: "/validate/"
	}
	
	Return obj
End Function

