' +---------------------------------------------------------------------------+
' | Skoobalon Personal Roku Channel                                           |
' | Copyright (C) 2013 Steven T. Wallace                                      |
' |                                                                           |
' | This program is free software; you can redistribute it and/or modify      |
' | it under the terms of the GNU General Public License as published by      |
' | the Free Software Foundation; version 2 of the License.                   |
' |                                                                           |
' | This program is distributed in the hope that it will be useful,           |
' | but WITHOUT ANY WARRANTY; without even the implied warranty of            |
' | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
' | GNU General Public License for more details.                              |
' |                                                                           |
' | You should have received a copy of the GNU General Public License along   |
' | with this program; if not, write to the Free Software Foundation, Inc.,   |
' | 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.               |
' +---------------------------------------------------------------------------+


' Screen which displays all photo albums on a data grid
Function PhotoAlbumGridScreen()
	this = {}
	
	' Constructor
	this.Init = Function()
		Print "PhotoAlbumGridScreen Init"
		m.port = CreateObject("roMessagePort")
		m.screen = CreateObject("roGridScreen")
		m.screen.SetMessagePort(m.port)
		m.screen.SetBreadcrumbText("Photography", "")
		m.screen.SetGridStyle("flat-landscape")
		m.screen.SetDisplayMode("scale-to-fit")
		m.screen.SetDescriptionVisible(false)
		
		' Load albums XML
		' albumsList is Array of Objects { title:string, subalbums:object }
		m.albumsList = m.LoadAlbumXml()
		albumNames = CreateObject("roArray", m.albumsList.Count(), true)
		if m.albumsList <> invalid
			print "Creating album lists with "; m.albumsList.Count(); " albums"
			' Define each album
			for i = 0 to m.albumsList.Count() - 1
				print "- "; m.albumsList[i].Title
				albumNames.Push(m.albumsList[i].Title)
			end for
			
			m.screen.AddHeader("Authorization", "Basic " + GetAuthPassword())
			m.screen.SetupLists(m.albumsList.Count())
			m.screen.SetListNames(albumNames)
			
			' Populate all albums, ensure first album is highlighted
			for i = m.albumsList.Count() - 1 to 0 step -1
				m.screen.SetContentList(i, m.albumsList[i].SubAlbums)
				m.screen.SetFocusedListItem(i, 0)
			end for
		end if
	End Function
	
	' Shows the screen
	' Returns once the user has left the screen
	this.Show = Function()
		Print "PhotoAlbumGridScreen Show"
		m.screen.Show()
		
		while true
			msg = wait(0, m.port)
			if type(msg) = "roGridScreenEvent" then
				if msg.IsScreenClosed()
					print "PhotoAlbumGridScreen closed"
					exit while
				else if msg.IsListItemFocused()
					Print "Selected row "; msg.GetIndex(); "col "; msg.GetData()
				else if msg.IsListItemSelected()
					' Item (subalbum) was selected
					' Show empty canvas
					black = CreateObject("roImageCanvas")
					black.Show()
					m.ShowSlideshow(msg.GetIndex(), msg.GetData())
					black.Close()
				end if
			end if
		end while
	End Function
	
	this.ShowSlideshow = Function (album as Integer, subalbum as Integer, startAtBack = false, showOverlay = false)
		pss = PhotoSlideshowScreen(m.albumsList[album].Title, m.albumsList[album].SubAlbums[subalbum], startAtBack)
		pss.SetOverlayEnabled(showOverlay)
		pss.Show()
		
		' When execution gets here, the slideshow has now been closed
		If pss.IsPlayPrevious()
			' Show subalbum - 1
			subalbum = subalbum - 1
			If subalbum = -1
				album = album + 1
				If album = m.albumsList.Count()
					album = 0
				End If
				subalbum = m.albumsList[album].SubAlbums.Count() - 1
			End If
			m.screen.SetFocusedList(album)
			m.screen.SetContentList(m.albumsList[album].SubAlbums)
			m.screen.SetFocusedListItem(subalbum)
			m.ShowSlideshow(album, subalbum, true, pss.IsOverlayEnabled())
		Else If pss.IsPlayNext()
			' Show subalbum + 1
			subalbum = subalbum + 1
			If subalbum = m.albumsList[album].SubAlbums.Count()
				subalbum = 0
				album = album - 1
				If album = -1
					album = 0
				End If
			End If
			'm.screen.SetContentList(m.albumsList[album].SubAlbums)
			m.screen.SetFocusedListItem(album, subalbum)
			m.ShowSlideshow(album, subalbum, false, pss.IsOverlayEnabled())
		End If
	End Function
	
	this.LoadAlbumXml = Function() As Object
		url = "http://" + GetSettings().host$ + "/photography/roku/roku.xml.php"
		print "Loading album XML from "; url
		http = NewHttp(url)
		response = http.GetToStringWithRetry()
		
		xml = CreateObject("roXMLElement")
		if not xml.Parse(response)
			print "Cannot parse feed"
			return invalid
		end if
		
		if xml.album = invalid then
			print "Improper root element"
			return invalid
		end if
		
		if islist(xml.album) = false then
			print "No albums found"
			return invalid
		end if
		
		if xml.album[0].GetName() <> "album" then
			print "No albums found"
			return invalid
		end if
		
		' Build the list used to build the grid view
		' albumList is an array of album objects {title:String, subalbums:Object}
		albumList = CreateObject("roArray", 10, true)
		albums = xml.GetChildElements()
		print "Extracting album details"
		for each album in albums
			albumObject = CreateObject("roAssociativeArray")
			albumObject.Title = album@name
			albumObject.Subalbums = CreateObject("roArray", 10, true)
			print albumObject.Title; "..."
			for each subalbum in album.GetChildElements()
				' Subalbum objects are defined as {title:String, description:String, galleryxml:String}
				o = CreateObject("roAssociativeArray")
				o.Path = subalbum@path
				o.Title = subalbum@name
				o.Description = subalbum@date
				o.ShortDescriptionLine1 = o.Title
				o.ShortDescriptionLine2 = o.Description
				o.GalleryXml = subalbum@xml
				o.HDPosterUrl = UrlEncode("http://" + GetSettings().host$ + subalbum@image)
				albumObject.Subalbums.Push(o)
				print "- "; o.Title; "; "; o.Description; "; " o.HDPosterUrl
			end for
			albumList.Push(albumObject)
		end for
		
		return albumList
	End Function

	this.Init()
	return this
End Function