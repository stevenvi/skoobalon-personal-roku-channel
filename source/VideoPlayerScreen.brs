' +---------------------------------------------------------------------------+
' | Skoobalon Personal Roku Channel                                           |
' | Copyright (C) 2013 Steven T. Wallace                                      |
' |                                                                           |
' | This program is free software; you can redistribute it and/or modify      |
' | it under the terms of the GNU General Public License as published by      |
' | the Free Software Foundation; version 2 of the License.                   |
' |                                                                           |
' | This program is distributed in the hope that it will be useful,           |
' | but WITHOUT ANY WARRANTY; without even the implied warranty of            |
' | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
' | GNU General Public License for more details.                              |
' |                                                                           |
' | You should have received a copy of the GNU General Public License along   |
' | with this program; if not, write to the Free Software Foundation, Inc.,   |
' | 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.               |
' +---------------------------------------------------------------------------+


Function VideoPlayerScreen(video)
	this = {}
	
	' Constructor
	this.Init = Function(video)
		Print "VideoPlayerScreen.Init(..)"
		m.port = CreateObject("roMessagePort")
		m.screen = CreateObject("roVideoScreen")
		m.screen.AddHeader("Authorization", "Basic " + GetAuthPassword())
		m.screen.SetMessagePort(m.port)
		m.screen.SetPositionNotificationPeriod(30)
		m.screen.SetContent(video)
	End Function
	
	this.Show = Function()
		Print "VideoPlayerScreen.Show()"
		m.screen.Show()
		m.ReadyForNext = false
		While True
			msg = wait(0, m.port)
			If Type(msg) = "roVideoScreenEvent"
				If msg.IsScreenClosed()
					Print "VideoPlayerScreen Closed"
					Exit While
				Else If msg.IsPlaybackPosition()
					'nowpos = msg.GetIndex()
					'RegWrite(video.ContentId, nowpos.toStr())
				Else If msg.IsFullResult()
					' Video playback has ended
					m.ReadyForNext = true
				End If
			End If
		End While
	End Function
	
	this.IsReadyForNext = Function() as Boolean
		return m.ReadyForNext
	End Function
	
	this.Init(video)
	return this
End Function
