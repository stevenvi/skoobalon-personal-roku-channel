' +---------------------------------------------------------------------------+
' | Skoobalon Personal Roku Channel                                           |
' | Copyright (C) 2013 Steven T. Wallace                                      |
' |                                                                           |
' | This program is free software; you can redistribute it and/or modify      |
' | it under the terms of the GNU General Public License as published by      |
' | the Free Software Foundation; version 2 of the License.                   |
' |                                                                           |
' | This program is distributed in the hope that it will be useful,           |
' | but WITHOUT ANY WARRANTY; without even the implied warranty of            |
' | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
' | GNU General Public License for more details.                              |
' |                                                                           |
' | You should have received a copy of the GNU General Public License along   |
' | with this program; if not, write to the Free Software Foundation, Inc.,   |
' | 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.               |
' +---------------------------------------------------------------------------+


Sub Main()
	Print "Starting Program"
	
	' Fade splash screen (placeholder is needed to prevent app from closing prematurely)
	placeholder = CreateObject("roScreen")
	ShowSplashScreen("pkg:/images/SplashScreen.jpg", 0.9, 0.75)
	
	' Validate Credentials
    InitTheme()
	s = GetSettings()
	done = true
	valid = s.Validate()
	While Not valid Or Not done
		' TODO: Show settings screen
		Print "Settings could not be validated, launching settings screen"
		done = NewSettingsScreen().Show()
		If done
			valid = s.Validate()
			If Not valid Then NewSimpleDialog("Error", "The provided credentials could not be validated.", false, ["OK"]).Show()
		End If
	End While
	
	' Show Main Menu: MediaTypeScreen
	NewMainMenuScreen().Show()
	
	' Main Menu Exited, Program Exits
	Print "Exiting program"
End Sub


'*************************************************************
'** Set the configurable theme attributes for the application
'** 
'** Configure the custom overhang and Logo attributes
'** Theme attributes affect the branding of the application
'** and are artwork, colors and offsets specific to the app
'*************************************************************

Sub InitTheme()

    app = CreateObject("roAppManager")
    theme = {
		BackgroundColor: "#333333",
		OverhangSliceHD: "pkg:/images/OverhangSliceHD.jpg",
		OverhangPrimaryLogoHD: "pkg:/images/Overhang_Logo_HD.png",
		OverhangPrimaryLogoOffsetHD_X: "125",
		OverhangPrimaryLogoOffsetHD_Y: "35"
	}

    app.SetTheme(theme)

End Sub
