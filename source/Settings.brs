' +---------------------------------------------------------------------------+
' | Skoobalon Personal Roku Channel                                           |
' | Copyright (C) 2013 Steven T. Wallace                                      |
' |                                                                           |
' | This program is free software; you can redistribute it and/or modify      |
' | it under the terms of the GNU General Public License as published by      |
' | the Free Software Foundation; version 2 of the License.                   |
' |                                                                           |
' | This program is distributed in the hope that it will be useful,           |
' | but WITHOUT ANY WARRANTY; without even the implied warranty of            |
' | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
' | GNU General Public License for more details.                              |
' |                                                                           |
' | You should have received a copy of the GNU General Public License along   |
' | with this program; if not, write to the Free Software Foundation, Inc.,   |
' | 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.               |
' +---------------------------------------------------------------------------+


' Singleton
Function GetSettings()
	globals = GetGlobalAA()
	If globals._settingsInstance = Invalid
		globals._settingsInstance = _NewSettings()
	End If
	Return globals._settingsInstance
End Function

' Creates a new Settings object, which holds and validates all settings
Function _NewSettings()
	Print "Settings constructor"
	globals = GetGlobalAA()
	
	? GetProperties().GetHostIndex()
	
	' Properties, populated with stored data
	obj = {
		username$: GetProperties().GetUsername()
		password$: GetProperties().GetPassword()
		host$: GetConstants().HOSTS[GetProperties().GetHostIndex()]
		ssl: false
	}
	
	' Methods
	obj.Validate = Function()
		Print "Settings.Validate()"
		
		If m.ssl
			protocol$ = "https"
		Else
			protocol$ = "http"
		End If
		
		validationUrl$ = protocol$ + "://" + m.host$ + GetConstants().VALIDATION_PATH
		Print "Validating against "; validationUrl$
		
		' TODO: Make request to determine permissions instead of assuming we have them all based on this one url
		testRequest = NewHttp(validationUrl$)
		If Not testRequest.Http.AsyncGetToString()
			print "Couldn't get test file to string, assuming bad credentials"
			Return False
		End If
		
		While True
			msg = Wait(0, testRequest.Http.GetMessagePort())
			Print "Response code: " msg.GetResponseCode()
			If msg.GetResponseCode() >= 400 OR msg.GetResponseCode() < 0
				Print "That's an error!"
				Return False
			Else
				Print "Assuming success!"
				Return True
			End If
		End While
	End Function ' Validate
	
	Return obj
End Function