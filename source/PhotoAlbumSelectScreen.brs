' +---------------------------------------------------------------------------+
' | Skoobalon Personal Roku Channel                                           |
' | Copyright (C) 2013 Steven T. Wallace                                      |
' |                                                                           |
' | This program is free software; you can redistribute it and/or modify      |
' | it under the terms of the GNU General Public License as published by      |
' | the Free Software Foundation; version 2 of the License.                   |
' |                                                                           |
' | This program is distributed in the hope that it will be useful,           |
' | but WITHOUT ANY WARRANTY; without even the implied warranty of            |
' | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
' | GNU General Public License for more details.                              |
' |                                                                           |
' | You should have received a copy of the GNU General Public License along   |
' | with this program; if not, write to the Free Software Foundation, Inc.,   |
' | 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.               |
' +---------------------------------------------------------------------------+


' Screen which displays all photo albums
Function PhotoAlbumSelectScreen(xmlFile$)
	obj = {
		_screen: CreateObject("roPosterScreen")
		_port:   CreateObject("roMessagePort")
	}
	
	' Construction
	Print "PhotoAlbumSelectScreen Init"
	obj._screen.SetMessagePort(obj._port)
	obj._screen.SetBreadcrumbText("Photography", "")
	obj._screen.SetListStyle("flat-category")
	obj._screen.SetListDisplayMode("zoom-to-fill")
	
	' Load albums XML
	' albumsList is Array of Objects { title:string, subalbums:object }
	obj.albumsList = _PhotoAlbumSelectScreen_LoadAlbumXml(xmlFile$)
	albumNames = CreateObject("roArray", obj.albumsList.Count(), True)
	If obj.albumsList <> Invalid
		print "Creating album lists with "; obj.albumsList.Count(); " albums"
		' Define each album
		For i = 0 to obj.albumsList.Count() - 1
			print "- "; obj.albumsList[i].Title
			albumNames.Push(obj.albumsList[i].Title)
		End For
		
		obj._screen.AddHeader("Authorization", "Basic " + GetAuthPassword())
		obj._screen.SetListNames(albumNames)
		
		' Show the first album
		obj._screen.SetContentList(obj.albumsList[0].SubAlbums)
		obj.selectedCategory = 0
	End If
	
	
	' Shows the screen
	' Returns once the user has left the screen
	obj.Show = Function() as Void
		Print "PhotoAlbumSelectScreen Show"
		m._screen.Show()
		
		while true
			msg = wait(0, m._port)
			if type(msg) = "roPosterScreenEvent" then
				if msg.IsScreenClosed()
					print "PhotoAlbumSelectScreen closed"
					exit while
				else if msg.IsListFocused()
					' Category (album) was focused
					m.selectedCategory = msg.GetIndex()
					print "Album Selected: "; m.selectedCategory
					
					' Define subalbums
					print "Defining subalbums"
					m._screen.SetContentList(m.albumsList[m.selectedCategory].SubAlbums)
					m._screen.SetFocusedListItem(0)
				else if msg.IsListItemSelected()
					' Item (subalbum) was selected
					' Show empty canvas
					black = CreateObject("roImageCanvas")
					black.Show()
					m.ShowSlideshow(m.selectedCategory, msg.GetIndex())
					black.Close()
				end if
			end if
		end while
	End Function
	
	obj.ShowSlideshow = Function (album as Integer, subalbum as Integer, startAtBack = false, showOverlay = false)
		pss = NewPhotoSlideshowScreen(m.albumsList[album].SubAlbums[subalbum], startAtBack)
		pss.SetOverlayEnabled(showOverlay)
		pss.Show()
		
		' When execution gets here, the slideshow has now been closed
		If pss.IsPlayPrevious()
			' Show subalbum - 1
			subalbum = subalbum - 1
			If subalbum = -1
				album = album + 1
				If album = m.albumsList.Count()
					album = 0
				End If
				subalbum = m.albumsList[album].SubAlbums.Count() - 1
			End If
			m._screen.SetFocusedList(album)
			m._screen.SetContentList(m.albumsList[album].SubAlbums)
			m._screen.SetFocusedListItem(subalbum)
			m.ShowSlideshow(album, subalbum, true, pss.IsOverlayEnabled())
		Else If pss.IsPlayNext()
			' Show subalbum + 1
			subalbum = subalbum + 1
			If subalbum = m.albumsList[album].SubAlbums.Count()
				subalbum = 0
				album = album - 1
				If album = -1
					album = 0
				End If
			End If
			m._screen.SetFocusedList(album)
			m._screen.SetContentList(m.albumsList[album].SubAlbums)
			m._screen.SetFocusedListItem(subalbum)
			m.ShowSlideshow(album, subalbum, false, pss.IsOverlayEnabled())
		End If
	End Function
	
	Return obj
End Function


Function _PhotoAlbumSelectScreen_LoadAlbumXml(xmlFile$) as Object
	url = "http://" + GetSettings().host$ + xmlFile$
	print "Loading album XML from "; url
	http = NewHttp(url)
	response = http.GetToStringWithRetry()
	
	xml = CreateObject("roXMLElement")
	If Not xml.Parse(response)
		Print "Cannot parse feed"
		Return Invalid
	End If
	
	If xml.album = Invalid
		Print "Improper root element"
		Return invalid
	End If
	
	' Build the list used to build the grid view
	' albumList is an array of album objects {title:String, subalbums:Object}
	albumList = CreateObject("roArray", 10, true)
	albums = xml.GetChildElements()
	Print "Extracting album details"
	For Each album in albums
		albumObject = CreateObject("roAssociativeArray")
		albumObject.Title = album@name
		albumObject.Subalbums = CreateObject("roArray", 10, true)
		print albumObject.Title; "..."
		For Each subalbum in album.GetChildElements()
			' Subalbum objects are defined as {title:String, description:String, galleryxml:String}
			o = CreateObject("roAssociativeArray")
			o.Path = subalbum@path
			o.Title = subalbum@name
			o.Description = subalbum@date
			o.ShortDescriptionLine1 = o.Title
			o.ShortDescriptionLine2 = o.Description
			o.GalleryXml = subalbum@xml
			o.HDPosterUrl = UrlEncode("http://" + GetSettings().host$ + subalbum@image)
			albumObject.Subalbums.Push(o)
			Print "- "; o.Title; "; "; o.Description; "; " o.HDPosterUrl
		End For
		albumList.Push(albumObject)
	End For
	
	Return albumList
End Function
