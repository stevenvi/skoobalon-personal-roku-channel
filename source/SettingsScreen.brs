' +---------------------------------------------------------------------------+
' | Skoobalon Personal Roku Channel                                           |
' | Copyright (C) 2013 Steven T. Wallace                                      |
' |                                                                           |
' | This program is free software; you can redistribute it and/or modify      |
' | it under the terms of the GNU General Public License as published by      |
' | the Free Software Foundation; version 2 of the License.                   |
' |                                                                           |
' | This program is distributed in the hope that it will be useful,           |
' | but WITHOUT ANY WARRANTY; without even the implied warranty of            |
' | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
' | GNU General Public License for more details.                              |
' |                                                                           |
' | You should have received a copy of the GNU General Public License along   |
' | with this program; if not, write to the Free Software Foundation, Inc.,   |
' | 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.               |
' +---------------------------------------------------------------------------+


' This is the settings screen that is actually used
Function NewSettingsScreen()
	Print "SettingsScreen constructor"
	obj = {}
	
	' Shows the settings screen, will close after any changes are made, because roParagraphScreens cannot have their contents changed
	' Returns true if the user closed it by clicking the "done" button, or false if it closed because an option was changed
	obj.Show = Function()
		print "SettingsScreen.Show()"
		m.scr = CreateObject("roParagraphScreen")
		m.port = CreateObject("roMessagePort")
		m.scr.SetMessagePort(m.port)
		m.scr.SetTitle("Configuration")
				
		m.scr.AddHeaderText("Credentials")
		m.scr.AddParagraph("Username: " + GetSettings().username$)
		m.scr.AddParagraph("Password: ********")
		m.scr.AddHeaderText("Settings")
		m.scr.AddParagraph("Host: " + GetSettings().host$)
		m.scr.AddButton(0, "Enter username")
		m.scr.AddButton(1, "Enter password")
		
		If GetConstants().HOSTS.Count() > 1
			m.scr.AddButton(2, "Change host")
		End If
		
		m.scr.AddButton(3, "Done")
		
		m.scr.Show()
		done = False
		While True
			msg = wait(0, m.port)
			Print "Message: "; msg
			If Type(msg) = "roParagraphScreenEvent"
				If msg.isButtonPressed()
					If msg.GetIndex() = 0
						m.EnterUsername()
					Else If msg.GetIndex() = 1
						m.EnterPassword()
					Else If msg.GetIndex() = 2
						print "Incrementing host index"
						m.IncrementHostIndex()
					Else If msg.GetIndex() = 3
						print "Done clicked"
						done = True
					End If
					m.scr.Close()
				Else If msg.isScreenClosed()
					Print "Screen closed"
					Exit While
				End If
			End If
		End While
		Print "Leaving settings screen ("; done; ")"
		
		Return done
	End Function
	
	obj.IncrementHostIndex = Function()
		index% = GetProperties().GetHostIndex() + 1
		maxIndex% = GetConstants().HOSTS.Count() - 1
		If index% > maxIndex%
			index% = 0
		End If
		GetProperties().SetHostIndex(index%)
	End Function
	
	obj.EnterUsername = Function()
		scr = CreateObject("roKeyboardScreen")
		prt = CreateObject("roMessagePort")
		scr.SetMessagePort(prt)
		scr.SetTitle("User Credentials")
		scr.SetText(GetProperties().GetUsername())
		scr.SetDisplayText("Enter Username")
		scr.AddButton(0, "OK")
		scr.AddButton(1, "Cancel")
		scr.Show()
		
		While True
			msg = wait(0, prt)
			If Type(msg) = "roKeyboardScreenEvent"
				If msg.isScreenClosed()
					Exit While
				Else If msg.isButtonPressed()
					If msg.GetIndex() = 0
						GetProperties().SetUsername(scr.GetText())
					End If
					Exit While
				End If
			End If
		End While
	End Function
	
	obj.EnterPassword = Function()
		scr = CreateObject("roKeyboardScreen")
		prt = CreateObject("roMessagePort")
		scr.SetMessagePort(prt)
		scr.SetTitle("User Credentials")
		scr.SetText(GetProperties().GetPassword())
		scr.SetDisplayText("Enter Password")
		scr.AddButton(0, "OK")
		scr.AddButton(1, "Cancel")
		scr.Show()
		
		While True
			msg = wait(0, prt)
			If Type(msg) = "roKeyboardScreenEvent"
				If msg.isScreenClosed()
					Exit While
				Else If msg.isButtonPressed()
					If msg.GetIndex() = 0
						GetProperties().SetPassword(scr.GetText())
					End If
					Exit While
				End If
			End If
		End While
	End Function
	
	return obj
End Function