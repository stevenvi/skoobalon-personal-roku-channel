' +---------------------------------------------------------------------------+
' | Skoobalon Personal Roku Channel                                           |
' | Copyright (C) 2013 Steven T. Wallace                                      |
' |                                                                           |
' | This program is free software; you can redistribute it and/or modify      |
' | it under the terms of the GNU General Public License as published by      |
' | the Free Software Foundation; version 2 of the License.                   |
' |                                                                           |
' | This program is distributed in the hope that it will be useful,           |
' | but WITHOUT ANY WARRANTY; without even the implied warranty of            |
' | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
' | GNU General Public License for more details.                              |
' |                                                                           |
' | You should have received a copy of the GNU General Public License along   |
' | with this program; if not, write to the Free Software Foundation, Inc.,   |
' | 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.               |
' +---------------------------------------------------------------------------+

'
' Creates a new PhotoSlideshowScreen object
'
' This object wil take in a SubAlbum object representing a Gallery/JuiceBox gallery xml file,
' parse all image data from it, and create an ImprovedSlideshow with it. Upon completion of
' the slideshow, either by wrapping to previous or next images, the slideshow is closed and
' flags are set denoting which direction the show ended at.
'
' tl;dr: This component shows a slideshow once then exits. It is up to you to create a new
' instance of this class afterwards to show more images, if desired.
'
Function NewPhotoSlideshowScreen(subAlbum as Object, startAtBack = false as Boolean) as Object
	Print "PhotoSlideshowScreen Constructor"
	obj = {
		_screen: Invalid
		_imageList: []
	}
	
	' Load the album XML
	url$ = "http://" + GetSettings().host$ + UrlEncode(subAlbum.GalleryXml)
	Print "Loading photo album from: "; url$
	http = NewHttp(url$)
	response = Http.GetToStringWithRetry()
	
	xml = CreateObject("roXMLElement")
	If Not xml.Parse(response)
		Print "Cannot parse xml"
		Return Invalid
	End If
	
	If xml.image = Invalid
		Print "No images found"
		Return Invalid
	End If

	' Build the list of images to use
	images = xml.GetChildElements()
	For i = 0 to images.Count() - 1
		image = images[i]
		obj._imageList.Push({
			Url: UrlEncode("http://" + GetSettings().host$ + "/photography/view/" + subAlbum.Path + "/" + image@imageURL)
			TextOverlayBody: image.description.GetText()
			TextOverlayUL: image.date.GetText()
			TextOverlayUR: subAlbum.Title + ":" + Stri(i + 1) + " of " + Stri(images.Count()).Trim()
		})
	End For

	' Set up the screen
	obj._screen = NewImprovedSlideshow()
	obj._screen.AddHeader("Authorization", "Basic " + GetAuthPassword())
	obj._screen.SetContentList(obj._imageList, true)
	obj._screen.SetPeriod(7.5)
	obj._screen.SetDisplayMode("scale-to-fit")

	If startAtBack
		Print "Starting at back"
		obj._screen.SetNext(images.Count() - 1, True)
	End If
	
	
	'''''''''''''''''''''''''''''''''''
	'
	' Methods
	'
	'''''''''''''''''''''''''''''''''''
	
	' Shows the slideshow
	obj.Show = Function()
		Print "PhotoSlideshowScreen.Show()"
		m._screen.Show()
		
		port = m._screen.GetMessagePort()
		While Not m._screen.IsClosed()
			msg = port.GetMessage()
			While msg <> Invalid
				' Pass non-overridden events through to the ImprovedSlideshow component
				m._screen.HandleMessage(msg)
				
				msg = port.GetMessage()
			End While
			
			m._screen.UpdateLogic()
			m._screen.Draw()
		End While
	End Function ' Show
	
	obj.IsPlayNext = Function() as Boolean
		If m.playNext <> Invalid
			Return m.playNext
		Else
			Return False
		End If
	End Function ' IsPlayNext
	
	obj.IsPlayPrevious = Function() as Boolean
		If m.playPrevious <> Invalid
			Return m.playPrevious
		Else
			Return False
		End If
	End Function ' IsPlayPrevious
	
	obj.IsOverlayEnabled = Function() as Boolean
		Return m._screen.IsTextOverlayVisible()
	End Function ' IsOverlayEnabled
	
	obj.SetOverlayEnabled = Function(value as Boolean)
		m._overlayVisible = value
		m._screen.SetTextOverlayVisible(m.IsOverlayEnabled())
	End Function ' SetOverlayEnabled
	
	Return obj
End Function ' NewPhotoSlideshowScreen
