' +---------------------------------------------------------------------------+
' | Skoobalon Personal Roku Channel                                           |
' | Copyright (C) 2013 Steven T. Wallace                                      |
' |                                                                           |
' | This program is free software; you can redistribute it and/or modify      |
' | it under the terms of the GNU General Public License as published by      |
' | the Free Software Foundation; version 2 of the License.                   |
' |                                                                           |
' | This program is distributed in the hope that it will be useful,           |
' | but WITHOUT ANY WARRANTY; without even the implied warranty of            |
' | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
' | GNU General Public License for more details.                              |
' |                                                                           |
' | You should have received a copy of the GNU General Public License along   |
' | with this program; if not, write to the Free Software Foundation, Inc.,   |
' | 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.               |
' +---------------------------------------------------------------------------+


'** Media Type Screen
'** Used to select which type of media the user wants to view
'** (Videos, Pictures)
'******************************************************************************

Function NewMainMenuScreen()
	Print "MainMenuScreen Constructor"
	
	obj = {}
		
	' Define the screen
	obj.port = CreateObject("roMessagePort")
	obj.screen = CreateObject("roListScreen")
	obj.screen.SetMessagePort(obj.port)
	obj.screen.SetBreadcrumbText("Main Menu", "")
	
	' Build the list of items
	CreateItem = Function(name)
		o = {
			Title: name
			ShortDescriptionLine1: name
			HDPosterUrl: "pkg:/images/" + name + ".png"
		}
		Return o
	End Function
	
	obj.screen.SetContent([
		CreateItem("Videos"),		' 0
		CreateItem("Photographs"),	' 1
'		CreateItem("Photo Grid"),	' 2
		CreateItem("Settings")		' 3
	])
	
	' Methods
	obj.Show = Function()
		Print "MainMenuScreen.Show()"
		m.screen.Show()

		While True
			msg = wait(0, m.port)
			If type(msg) = "roListScreenEvent"
				If msg.IsScreenClosed()
					Print "MainMenuScreen closed"
					Exit While
				Else If msg.isRemoteKeyPressed()
					Print "Remote key pressed: "; msg.GetIndex()
				Else If msg.isListItemSelected()
					If msg.GetIndex() = 0
						VideoSelectListScreen("/roku/dvds.xml.php", "Videos").Show()
					Else If msg.GetIndex() = 1
						Print "Pictures Section Selected"
						PhotoAlbumSelectScreen("/photography/roku/roku.xml.php").Show()
'					Else If msg.GetIndex() = 2
'						Print "Photo Grid Selected"
'						PhotoAlbumGridScreen().Show()
					Else If msg.GetIndex() = 2
						Print "Settings screen selected"
						done = false
						s = GetSettings()
						blankScreen = CreateObject("roImageCanvas")
						blankScreen.Show()
						valid = s.Validate()
						While Not valid Or Not done
							done = NewSettingsScreen().Show()
							If done 
								valid = s.Validate()
								If Not valid Then NewSimpleDialog("Error", "The credentials provided could not be validated.", false, ["OK"]).Show()
							End If
						End While
						blankScreen.Close()
					End If
				End If
			End If
		End While
	End Function ' Show

	
	Return obj
End Function
