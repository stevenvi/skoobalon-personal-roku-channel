# README #

The Skoobalon Personal Roku Channel is a base channel which, given an xml description file, can stream videos and display photo galleries hosted on an external web server. This channel is for anyone who has implemented their own personal web-accessible home movie or photo galleries that wish to also view this content through their Roku 3 or later.

This is a constantly evolving project, as my needs change. Currently I use this channel to view my personal home movies, my DVD and Blu-ray collections, and view all of my family photo albums. I provide xml files to index all my content on my personal web server, and the channel does the rest in terms of displaying things.

### License ###

This software is licensed under the GNU General Public License v2. See the gpl-2.0.txt file in the repository for more details.

### How do I get set up? ###

* Prerequisites: A Roku 3 or higher, a web server that you have access to, and ideally a Linux machine for development.
* Place your Roku in developer mode. This is left as an exercise to the reader, there's lots of good documentation on using the Roku out there.
* Update source/Globals.brs to set the host of your server and validation URL, if applicable.
* On your Linux machine, add the following properties to your shell:

```
#!bash

export ROKU_DEV_TARGET=<IP of your Roku>
export DEVPASSWORD=<Your Roku's developer password>

```

* Run "make install" and the program should install on your Roku automatically. (You may need to install make and a zip utility first.)
* All source code is in the source directory. Enjoy!